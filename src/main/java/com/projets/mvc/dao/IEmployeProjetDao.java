package com.projets.mvc.dao;

import com.projets.mvc.entities.EmployeProjet;

public interface IEmployeProjetDao extends IGenericDao<EmployeProjet> {

}

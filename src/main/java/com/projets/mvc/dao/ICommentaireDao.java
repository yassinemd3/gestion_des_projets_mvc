package com.projets.mvc.dao;

import com.projets.mvc.entities.Commentaire;

public interface ICommentaireDao extends IGenericDao<Commentaire> {

}

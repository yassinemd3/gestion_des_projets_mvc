package com.projets.mvc.dao;

import com.projets.mvc.entities.EmployeTache;

public interface IEmployeTacheDao extends IGenericDao<EmployeTache> {

}

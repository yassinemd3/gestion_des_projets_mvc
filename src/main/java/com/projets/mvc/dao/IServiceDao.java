package com.projets.mvc.dao;

import com.projets.mvc.entities.Service;

public interface IServiceDao extends IGenericDao<Service>{

}

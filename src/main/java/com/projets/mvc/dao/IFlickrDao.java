package com.projets.mvc.dao;

import java.io.InputStream;

public interface IFlickrDao {
	public String savePhoto(InputStream photo, String titre) throws Exception;

}

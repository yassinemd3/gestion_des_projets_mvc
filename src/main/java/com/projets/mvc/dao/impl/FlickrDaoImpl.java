package com.projets.mvc.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuth1Token;
import com.projets.mvc.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao {

	private Flickr flickr;
	private UploadMetaData uploadMetaData = new UploadMetaData();
	private String apiKey = "bb42253dfac7cc90128dcb5f173b879b";
	private String sharedSecret = "9268bb849c23ef87";
	
	private void connect(){
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157714492889776-2d9df84639611166");
		auth.setTokenSecret("f7f8baca5316042c");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}

	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		connect();
		uploadMetaData.setTitle(title);
		String photoId = flickr.getUploader().upload(photo, uploadMetaData);	
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
		
	}
	public void auth(){
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		AuthInterface authInterface = flickr.getAuthInterface();
		OAuth1RequestToken token = authInterface.getRequestToken();
		System.out.println("token: " + token);
		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this URL to authorise yourself on Flickr");
		System.out.println(url);
		System.out.println("Paste in the token it gives you");
		System.out.println(">>");
		String tokenKey = JOptionPane.showInputDialog(null);
		OAuth1Token requestToken = authInterface.getAccessToken(token, tokenKey);
		System.out.println("Authentificaton success");
		Auth auth = null;
		try{
			auth = authInterface.checkToken(requestToken);
		}catch (FlickrException e) {
			e.printStackTrace();
		}
		System.out.println("Token: " + requestToken.getToken());
		System.out.println("Secret: "+ requestToken.getTokenSecret());
		System.out.println("nsid: " + auth.getUser().getId());
		System.out.println("Realname: "+ auth.getUser().getRealName());
		System.out.println("Username: "+ auth.getUser().getUsername());
	}
	
	
}

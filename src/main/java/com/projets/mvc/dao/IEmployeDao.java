package com.projets.mvc.dao;

import com.projets.mvc.entities.Employe;

public interface IEmployeDao extends IGenericDao<Employe> {

}

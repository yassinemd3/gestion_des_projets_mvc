package com.projets.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.projets.mvc.dao.IServiceDao;
import com.projets.mvc.entities.Service;
import com.projets.mvc.services.IServiceService;
@Transactional
public class ServiceServiceImpl implements IServiceService{
	
	private IServiceDao dao;
	
	public void setDao(IServiceDao dao) {
		this.dao = dao;
	}

	@Override
	public Service save(Service entity) {
		return dao.save(entity);
	}

	@Override
	public Service update(Service entity) {
		return dao.update(entity);
	}

	@Override
	public List<Service> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Service> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Service getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Service findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Service findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}

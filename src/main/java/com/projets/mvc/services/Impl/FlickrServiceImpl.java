package com.projets.mvc.services.Impl;

import java.io.InputStream;

import com.projets.mvc.dao.IFlickrDao;
import com.projets.mvc.services.IFlickerService;

public class FlickrServiceImpl implements IFlickerService{
	
	private IFlickrDao dao;
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}
	@Override
	public String savePhoto(InputStream photo, String titre) throws Exception {
		return dao.savePhoto(photo, titre);
	}

	

}

package com.projets.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.projets.mvc.dao.IProjetsDao;
import com.projets.mvc.entities.Projets;
import com.projets.mvc.services.IProjetsService;
@Transactional
public class ProjetsServiceImpl implements IProjetsService{
	
	private IProjetsDao dao;
	
	public void setDao(IProjetsDao dao) {
		this.dao = dao;
	}

	@Override
	public Projets save(Projets entity) {
		return dao.save(entity);
	}

	@Override
	public Projets update(Projets entity) {
		return dao.update(entity);
	}

	@Override
	public List<Projets> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Projets> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Projets getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Projets findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Projets findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}

package com.projets.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.projets.mvc.dao.IEmployeProjetDao;
import com.projets.mvc.entities.EmployeProjet;
import com.projets.mvc.services.IEmployeProjetService;
@Transactional
public class EmployeProjetServiceImpl implements IEmployeProjetService{
	
	private IEmployeProjetDao dao;
	
	public void setDao(IEmployeProjetDao dao) {
		this.dao = dao;
	}

	@Override
	public EmployeProjet save(EmployeProjet entity) {
		return dao.save(entity);
	}

	@Override
	public EmployeProjet update(EmployeProjet entity) {
		return dao.update(entity);
	}

	@Override
	public List<EmployeProjet> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<EmployeProjet> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public EmployeProjet getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public EmployeProjet findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public EmployeProjet findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}

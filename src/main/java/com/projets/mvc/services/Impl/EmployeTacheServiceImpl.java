package com.projets.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.projets.mvc.dao.IEmployeTacheDao;
import com.projets.mvc.entities.EmployeTache;
import com.projets.mvc.services.IEmployeTacheService;
@Transactional
public class EmployeTacheServiceImpl implements IEmployeTacheService{
	
	private IEmployeTacheDao dao;
	
	public void setDao(IEmployeTacheDao dao) {
		this.dao = dao;
	}

	@Override
	public EmployeTache save(EmployeTache entity) {
		return dao.save(entity);
	}

	@Override
	public EmployeTache update(EmployeTache entity) {
		return dao.update(entity);
	}

	@Override
	public List<EmployeTache> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<EmployeTache> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public EmployeTache getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public EmployeTache findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public EmployeTache findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}

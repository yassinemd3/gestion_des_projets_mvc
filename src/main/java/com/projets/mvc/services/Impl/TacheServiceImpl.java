package com.projets.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.projets.mvc.dao.ITacheDao;
import com.projets.mvc.entities.Tache;
import com.projets.mvc.services.ITacheService;
@Transactional
public class TacheServiceImpl implements ITacheService{
	
	private ITacheDao dao;
	
	public void setDao(ITacheDao dao) {
		this.dao = dao;
	}

	@Override
	public Tache save(Tache entity) {
		return dao.save(entity);
	}

	@Override
	public Tache update(Tache entity) {
		return dao.update(entity);
	}

	@Override
	public List<Tache> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Tache> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Tache getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Tache findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Tache findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}

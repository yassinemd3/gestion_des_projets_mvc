package com.projets.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.projets.mvc.dao.ICommentaireDao;
import com.projets.mvc.entities.Commentaire;
import com.projets.mvc.services.ICommentaireService;
@Transactional
public class CommentaireServiceImpl implements ICommentaireService{
	
	private ICommentaireDao dao;
	
	public void setDao(ICommentaireDao dao) {
		this.dao = dao;
	}

	@Override
	public Commentaire save(Commentaire entity) {
		return dao.save(entity);
	}

	@Override
	public Commentaire update(Commentaire entity) {
		return dao.update(entity);
	}

	@Override
	public List<Commentaire> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Commentaire> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Commentaire getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Commentaire findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Commentaire findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}

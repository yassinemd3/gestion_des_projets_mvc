package com.projets.mvc.services;

import java.util.List;

import com.projets.mvc.entities.Projets;;

public interface IProjetsService {
	public Projets save(Projets entity);
	public Projets update(Projets entity);
	public List<Projets> selectAll();
	public List<Projets> selectAll(String sortField, String sort);
	public Projets getById(Long id);
	public void remove(Long id);
	public Projets findOne(String paramName, Object paramValue);
	public Projets findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

package com.projets.mvc.services;

import java.util.List;

import com.projets.mvc.entities.EmployeTache;

public interface IEmployeTacheService {
	public EmployeTache save(EmployeTache entity);
	public EmployeTache update(EmployeTache entity);
	public List<EmployeTache> selectAll();
	public List<EmployeTache> selectAll(String sortField, String sort);
	public EmployeTache getById(Long id);
	public void remove(Long id);
	public EmployeTache findOne(String paramName, Object paramValue);
	public EmployeTache findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

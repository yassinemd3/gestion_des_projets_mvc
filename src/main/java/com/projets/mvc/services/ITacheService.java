package com.projets.mvc.services;

import java.util.List;

import com.projets.mvc.entities.Tache;

public interface ITacheService {
	public Tache save(Tache entity);
	public Tache update(Tache entity);
	public List<Tache> selectAll();
	public List<Tache> selectAll(String sortField, String sort);
	public Tache getById(Long id);
	public void remove(Long id);
	public Tache findOne(String paramName, Object paramValue);
	public Tache findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

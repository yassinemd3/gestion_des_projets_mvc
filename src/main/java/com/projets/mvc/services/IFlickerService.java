package com.projets.mvc.services;

import java.io.InputStream;

public interface IFlickerService {
	public String savePhoto(InputStream photo, String titre) throws Exception;
}

package com.projets.mvc.services;

import java.util.List;

import com.projets.mvc.entities.EmployeProjet;;

public interface IEmployeProjetService {
	public EmployeProjet save(EmployeProjet entity);
	public EmployeProjet update(EmployeProjet entity);
	public List<EmployeProjet> selectAll();
	public List<EmployeProjet> selectAll(String sortField, String sort);
	public EmployeProjet getById(Long id);
	public void remove(Long id);
	public EmployeProjet findOne(String paramName, Object paramValue);
	public EmployeProjet findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

package com.projets.mvc.services;

import java.util.List;

import com.projets.mvc.entities.Commentaire;

public interface ICommentaireService {
	public Commentaire save(Commentaire entity);
	public Commentaire update(Commentaire entity);
	public List<Commentaire> selectAll();
	public List<Commentaire> selectAll(String sortField, String sort);
	public Commentaire getById(Long id);
	public void remove(Long id);
	public Commentaire findOne(String paramName, Object paramValue);
	public Commentaire findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

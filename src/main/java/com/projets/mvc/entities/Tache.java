package com.projets.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Tache implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long idTache;
	private String descriptionT;
	private Date deadlineT;
	@ManyToOne
	@JoinColumn(name = "idProjet")
	private Projets projet;
	@OneToMany(mappedBy = "tache")
	private List<Commentaire> listComT;
	@OneToMany(mappedBy = "tache")
	private List<EmployeTache> listET;
	public Tache() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdTache() {
		return idTache;
	}
	public void setIdTache(Long idTache) {
		this.idTache = idTache;
	}
	public String getDescriptionT() {
		return descriptionT;
	}
	public void setDescriptionT(String descriptionT) {
		this.descriptionT = descriptionT;
	}
	public Date getDeadlineT() {
		return deadlineT;
	}
	public void setDeadlineT(Date deadlineT) {
		this.deadlineT = deadlineT;
	}
	public Projets getProjet() {
		return projet;
	}
	public void setProjet(Projets projet) {
		this.projet = projet;
	}
	public List<Commentaire> getListComT() {
		return listComT;
	}
	public void setListComT(List<Commentaire> listComT) {
		this.listComT = listComT;
	}
	public List<EmployeTache> getListET() {
		return listET;
	}
	public void setListET(List<EmployeTache> listET) {
		this.listET = listET;
	}

	}

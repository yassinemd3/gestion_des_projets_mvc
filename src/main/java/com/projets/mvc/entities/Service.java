package com.projets.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Service implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	@Id
	@GeneratedValue
	private Long idService;
	private String nameService;
	@OneToMany(mappedBy = "service")
	private List<Employe> listEmp;
	
	public Service() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdService() {
		return idService;
	}
	public void setIdService(Long idService) {
		this.idService = idService;
	}
	public String getNameService() {
		return nameService;
	}
	public void setNameService(String nameService) {
		this.nameService = nameService;
	}
	public List<Employe> getListEmp() {
		return listEmp;
	}
	public void setListEmp(List<Employe> listEmp) {
		this.listEmp = listEmp;
	}
	
}

package com.projets.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class EmployeProjet implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long idEmployeProjet;
	@ManyToOne
	@JoinColumn(name = "idProjet")
	private Tache projet;
	@ManyToOne
	@JoinColumn(name = "idEmploye")
	private Employe employe;
	public EmployeProjet() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdEmployeProjet() {
		return idEmployeProjet;
	}
	public void setIdEmployeProjet(Long idEmployeProjet) {
		this.idEmployeProjet = idEmployeProjet;
	}
	public Tache getProjet() {
		return projet;
	}
	public void setProjet(Tache projet) {
		this.projet = projet;
	}
	public Employe getEmploye() {
		return employe;
	}
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	
}

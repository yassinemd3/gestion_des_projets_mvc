package com.projets.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class EmployeTache implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long idEmployeTache;
	@ManyToOne
	@JoinColumn(name = "idTache")
	private Tache tache;
	@ManyToOne
	@JoinColumn(name = "idEmploye")
	private Employe employe;
	public EmployeTache() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdEmployeTache() {
		return idEmployeTache;
	}
	public void setIdEmployeTache(Long idEmployeTache) {
		this.idEmployeTache = idEmployeTache;
	}
	public Tache getTache() {
		return tache;
	}
	public void setTache(Tache tache) {
		this.tache = tache;
	}
	public Employe getEmploye() {
		return employe;
	}
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}


}

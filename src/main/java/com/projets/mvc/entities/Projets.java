package com.projets.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Projets implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long idProjet;
	private String titreP;
	private String descriptionP;
	private Date deadlineP;
	@OneToMany(mappedBy = "projet")
	private List<Tache> listTache;
	@OneToMany(mappedBy = "projet")
	private List<EmployeProjet> listEP;
	public Projets() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdProjet() {
		return idProjet;
	}
	public void setIdProjet(Long idProjet) {
		this.idProjet = idProjet;
	}
	public String getTitreP() {
		return titreP;
	}
	public void setTitreP(String titreP) {
		this.titreP = titreP;
	}
	public String getDescriptionP() {
		return descriptionP;
	}
	public void setDescriptionP(String descriptionP) {
		this.descriptionP = descriptionP;
	}
	public Date getDeadlineP() {
		return deadlineP;
	}
	public void setDeadlineP(Date deadlineP) {
		this.deadlineP = deadlineP;
	}
	public List<Tache> getListTache() {
		return listTache;
	}
	public void setListTache(List<Tache> listTache) {
		this.listTache = listTache;
	}
	public List<EmployeProjet> getListEP() {
		return listEP;
	}
	public void setListEP(List<EmployeProjet> listEP) {
		this.listEP = listEP;
	}


}

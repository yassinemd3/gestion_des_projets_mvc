package com.projets.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Commentaire implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long idCommentaire;
	private String titreCommentaire;
	private String descriptionP;
	@ManyToOne
	@JoinColumn(name = "idTache")
	private Tache tache;
	@ManyToOne
	@JoinColumn(name = "idEmploye")
	private Employe employe;
	public Commentaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdCommentaire() {
		return idCommentaire;
	}
	public void setIdCommentaire(Long idCommentaire) {
		this.idCommentaire = idCommentaire;
	}
	public String getTitreCommentaire() {
		return titreCommentaire;
	}
	public void setTitreCommentaire(String titreCommentaire) {
		this.titreCommentaire = titreCommentaire;
	}
	public String getDescriptionP() {
		return descriptionP;
	}
	public void setDescriptionP(String descriptionP) {
		this.descriptionP = descriptionP;
	}
	public Tache getTache() {
		return tache;
	}
	public void setTache(Tache tache) {
		this.tache = tache;
	}

}
